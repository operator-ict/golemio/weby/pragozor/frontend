export interface EnvDataModel {
  cmsUrl: string;
  apiUrl: string;
  recaptchaSiteKey: string;
  googleAnalyticsID: string;
  googleAnalyticsCookielessID: string;
  prahaPracujeUrl: string;
  hotjarID: string;
  hotjarVersion: number;
  PragueInNumbersUrl: string;
  ProjectsInPragueUrl: string;
}
