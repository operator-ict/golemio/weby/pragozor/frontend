import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CuriosityRoutingModule } from './curiosity-routing.module';
import * as components from './components';
import * as services from './services';
import { SharedModule } from '@shared/shared.module';

const COMPONENTS = [components.CuriosityComponent];

const SERVICES = [services.CuriosityService];

@NgModule({
  declarations: [...COMPONENTS],
  providers: [...SERVICES],
  imports: [SharedModule, CommonModule, CuriosityRoutingModule],
})
export class CuriosityModule {}
