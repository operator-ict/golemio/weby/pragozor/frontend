import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EconomyComponent } from './components/economy/economy.component';

const routes: Routes = [{ path: '', component: EconomyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EconomyRoutingModule {}
