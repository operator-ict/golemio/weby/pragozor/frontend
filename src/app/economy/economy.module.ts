import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EconomyRoutingModule } from './economy-routing.module';
import * as components from './components';
import * as services from './services';
import { SharedModule } from '@shared/shared.module';

const COMPONENTS = [components.EconomyComponent];

const SERVICES = [services.EconomyService];

@NgModule({
  declarations: [...COMPONENTS],
  providers: [...SERVICES],
  imports: [SharedModule, CommonModule, EconomyRoutingModule],
})
export class EconomyModule {}
