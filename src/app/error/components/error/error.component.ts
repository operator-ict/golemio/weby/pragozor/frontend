import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  AfterViewInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of, combineLatest } from 'rxjs';
import { map, filter, delay, concatMap, takeUntil } from 'rxjs/operators';
import { StateService } from '@core/services';
import { fadeInOutLoadingAnimation } from '@shared/animations';
import { BaseComponent } from '@shared/components';

@Component({
  selector: 'pr-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [fadeInOutLoadingAnimation],
})
export class ErrorComponent extends BaseComponent
  implements OnInit, AfterViewInit {
  context$: Observable<{
    isLoadingVisible: boolean;
    errorCode: number;
    errorText: string;
  }>;

  constructor(activatedRoute: ActivatedRoute, private state: StateService) {
    super();
    state.setLoadingVisibility(true);
    this.context$ = combineLatest([
      activatedRoute.data,
      state.state$.pipe(map((x) => x.loadingVisible)),
    ]).pipe(
      filter(
        ([data, isLoadingVisible]) =>
          data.errorCode != null && data.errorText != null
      ),
      map(([data, isLoadingVisible]) => {
        return {
          isLoadingVisible,
          errorCode: data.errorCode,
          errorText: data.errorText,
        };
      })
    );
  }

  ngOnInit(): void {}

  ngAfterViewInit() {
    of(true)
      .pipe(delay(1000), takeUntil(this.baseSubject))
      .subscribe(() => this.state.setLoadingVisibility(false));
  }

  reloadPage() {
    location.href = '/';
  }
}
