import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HabitationRoutingModule } from './habitation-routing.module';
import * as components from './components';
import * as services from './services';
import { SharedModule } from '@shared/shared.module';

const COMPONENTS = [components.HabitationComponent];

const SERVICES = [services.HabitationService];

@NgModule({
  declarations: [...COMPONENTS],
  providers: [...SERVICES],
  imports: [SharedModule, CommonModule, HabitationRoutingModule],
})
export class HabitationModule {}
