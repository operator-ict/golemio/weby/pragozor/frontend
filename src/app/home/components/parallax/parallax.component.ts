import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'pr-parallax',
  templateUrl: './parallax.component.html',
  styleUrls: ['./parallax.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParallaxComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
