import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NaturalEnvironmentComponent } from './components/natural-environment/natural-environment.component';

const routes: Routes = [{ path: '', component: NaturalEnvironmentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NaturalEnvironmentRoutingModule {}
