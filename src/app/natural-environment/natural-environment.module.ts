import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NaturalEnvironmentRoutingModule } from './natural-environment-routing.module';

import * as components from './components';
import * as services from './services';
import { SharedModule } from '@shared/shared.module';

const COMPONENTS = [components.NaturalEnvironmentComponent];

const SERVICES = [services.NaturalEnvironmentService];

@NgModule({
  declarations: [...COMPONENTS],
  providers: [...SERVICES],
  imports: [SharedModule, CommonModule, NaturalEnvironmentRoutingModule],
})
export class NaturalEnvironmentModule {}
