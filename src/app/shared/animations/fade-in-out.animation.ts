import { trigger, transition, style, animate } from '@angular/animations';

export const fadeInOutAnimation = trigger('fadeInOut', [
  transition(':enter', [
    // :enter is alias to 'void => *'
    style({ opacity: 0 }),
    animate(500, style({ opacity: 1 })),
  ]),
  transition(':leave', [
    // :leave is alias to '* => void'
    animate(500, style({ opacity: 0 })),
  ]),
]);

export const fadeInOutLoadingAnimation = trigger('fadeInOutLoading', [
  transition(':leave', [
    // :leave is alias to '* => void'
    animate(500, style({ opacity: 0 })),
  ]),
]);
