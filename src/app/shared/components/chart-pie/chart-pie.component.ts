import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  AfterViewInit,
  HostBinding,
  ViewChild,
  ElementRef,
  forwardRef,
} from '@angular/core';
import { WidgetModel, ChartPieDataModel } from '@shared/models/widgets';
import { CmsWidgetSettingsModel } from '@shared/models';
import { PieChartComponent, ColorHelper } from '@swimlane/ngx-charts';
import { WidgetWithLegendComponent } from '../base';

@Component({
  selector: 'pr-chart-pie',
  templateUrl: './chart-pie.component.html',
  styleUrls: ['./chart-pie.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: WidgetWithLegendComponent,
      useExisting: forwardRef(() => ChartPieComponent),
    },
  ],
})
export class ChartPieComponent extends WidgetWithLegendComponent
  implements OnInit, AfterViewInit {
  @ViewChild(PieChartComponent) chart: PieChartComponent;
  @ViewChild('legend', { read: ElementRef, static: true })
  legendComponent: ElementRef;
  @Input() settings: CmsWidgetSettingsModel;
  @HostBinding('style.height.px')
  get height() {
    return this.settings?.chartHeight;
  }
  // tslint:disable-next-line:variable-name
  private _data: WidgetModel<ChartPieDataModel>;
  @Input() set data(data: WidgetModel<ChartPieDataModel>) {
    this._data = data;

    this.legendData = data.data.data.map((d) => d.name);
    this.legendColors = new ColorHelper(
      this.settings.colorScheme,
      'ordinal',
      this.legendData,
      null
    );
  }
  get data(): WidgetModel<ChartPieDataModel> {
    return this._data;
  }

  legendData;
  legendColors;
  view: any[] = undefined;

  // options
  gradient = true;
  showLegend = true;
  showLabels = false;
  isDoughnut = false;
  tooltipDisabled = false;
  showDataLabel = false;
  legendPosition = 'below';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA'],
  };
  get legendHeight(): number {
    return (
      this.legendComponent?.nativeElement.offsetHeight ||
      this.settings.legendMinHeight
    );
  }

  constructor() {
    super();
  }

  ngOnInit() {}

  ngAfterViewInit() {}

  onSelect(data): void {}

  onActivate(data): void {}

  onDeactivate(data): void {}
  legendActivate(data) {
    this.chart.onActivate(data);
  }

  legendDeactivate(data) {
    this.chart.onDeactivate(data);
  }
}
