import { CmsLocalizedTextModel } from './cms-localized-text.model';

export interface CmsHeaderSliderItemModel {
  image: string;
  paragraph1: CmsLocalizedTextModel;
  paragraph2: CmsLocalizedTextModel;
  paragraph3: CmsLocalizedTextModel;
  order: number;
}
