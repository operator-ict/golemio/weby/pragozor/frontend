import { CmsContentModel } from './cms-content.model';
import { CmsColModel } from './cms-col.model';

export interface CmsRowModel {
  cols: CmsColModel[];
}
