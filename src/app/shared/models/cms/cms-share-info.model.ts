export interface CmsShareInfoModel {
  source: string;
  sourceUrl: string;
  updatedAt: Date;
  widgetId: string;
  refreshInterval: number;
  isLiveData: boolean;
}
