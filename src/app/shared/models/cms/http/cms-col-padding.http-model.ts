export interface CmsColPaddingHttpModel {
  top: number;
  left: number;
  right: number;
  bottom: number;
}
