import { CmsContentType } from './cms-content.type';
import { CmsCrawlerDataHttpModel } from './cms-crawler-data.http-model';
import { CmsPrahaPracujeWidgetHttpModel } from './cms-prahapracujewidget.http-model';
import { CmsTextHttpModel } from './cms-text.http-model';
import { CmsWidgetSettingsHttpModel } from './cms-widget-settings.http-model';

export interface CmsContentHttpModel {
  image?: string;
  prahaPracujeWidget?: CmsPrahaPracujeWidgetHttpModel;
  text?: CmsTextHttpModel;
  widget?: CmsWidgetSettingsHttpModel;
  crawler_data?: CmsCrawlerDataHttpModel;
  content_type: CmsContentType;
  order: number;
}
