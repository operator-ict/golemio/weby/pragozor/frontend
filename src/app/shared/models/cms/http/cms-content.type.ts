// TODO Jakub add new type: crawler-data
export type CmsContentType =
  | 'image'
  | 'text'
  | 'widget'
  | 'share-info'
  | 'crawler-data'
  | 'prahapracujewidget';
