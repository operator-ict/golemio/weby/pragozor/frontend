import { CmsTextHttpModel } from './cms-text.http-model';
import { CmsLocalizedTextHttpModel } from './cms-localized-text.http-model';

export interface CmsHeaderSliderItemHttpModel {
  image: string;
  paragraph1: CmsLocalizedTextHttpModel;
  paragraph2: CmsLocalizedTextHttpModel;
  paragraph3: CmsLocalizedTextHttpModel;
  order: number;
}
