export type CmsLegendPositionType = 'left' | 'center' | 'right';
