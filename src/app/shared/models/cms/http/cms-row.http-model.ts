import { CmsColHttpModel } from './cms-col.http-model';

export interface CmsRowHttpModel {
  cols: CmsColHttpModel[];
}
