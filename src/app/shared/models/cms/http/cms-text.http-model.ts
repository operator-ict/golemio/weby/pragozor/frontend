import { CmsTextType } from './cms-text.type';
import { CmsLocalizedTextHttpModel } from './cms-localized-text.http-model';
import { CmsTextAlignmentType } from './cms-text-alignment.type';

export interface CmsTextHttpModel {
  text: CmsLocalizedTextHttpModel;
  type: CmsTextType;
  alignment: CmsTextAlignmentType | CmsTextAlignmentType[];
}
