export type CmsTextType = 'h1' | 'h2' | 'h3' | 'h4' | 'paragraph' | 'link';
