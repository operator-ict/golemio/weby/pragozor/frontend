import { ChartSeriesGroupModel } from './chart-series-group.model';

export interface ChartDataModel {
  xLabel: string;
  yLabel: string;
  series: ChartSeriesGroupModel[];
}
