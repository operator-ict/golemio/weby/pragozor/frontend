import { CmsLocalizedTextHttpModel, CmsLocalizedTextModel } from '../cms';
import { ChartNameValueModel } from './chart-name-value.model';

export interface ChartGaugeDataModel {
  min: number;
  max: number;
  unit: CmsLocalizedTextHttpModel;
  data: ChartNameValueModel[];
}
