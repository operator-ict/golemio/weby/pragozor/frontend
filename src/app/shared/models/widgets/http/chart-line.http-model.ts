import { ChartMetadataHttpModel, WidgetHttpModel } from './common';

export interface ChartLineHttpModel
  extends WidgetHttpModel<ChartMetadataHttpModel, any[]> {}
