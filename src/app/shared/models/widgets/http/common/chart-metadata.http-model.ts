import { BaseMetadataHttpModel } from './base-metadata.http-model';
import { AxisXHttpModel } from './axis-x.http-model';
import { AxisYHttpModel } from './axis-y.http-model';

export interface ChartMetadataHttpModel extends BaseMetadataHttpModel {
  axis: {
    x: AxisXHttpModel;
    y: AxisYHttpModel;
  };
}
