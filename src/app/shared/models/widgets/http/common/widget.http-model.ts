import { BaseMetadataHttpModel } from '.';

export interface WidgetHttpModel<
  TMetadata extends BaseMetadataHttpModel,
  TData
> {
  id: string;
  metadata: TMetadata;
  data: TData;
}
