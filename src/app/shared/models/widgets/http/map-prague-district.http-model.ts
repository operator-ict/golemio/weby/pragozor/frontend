import { WidgetHttpModel, BaseMetadataHttpModel } from './common';

export interface MapPragueDistrictHttpModel
  extends WidgetHttpModel<
    BaseMetadataHttpModel,
    {
      district_id: string;
      title: string;
      value: number;
      unit: string;
      unit_en: string;
    }[]
  > {}
