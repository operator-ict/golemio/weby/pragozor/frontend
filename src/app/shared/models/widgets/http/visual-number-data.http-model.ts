export interface VisualNumberDataHttpModel {
  value: string;
  unit: string;
  label: string;
  label_en: string;
}
