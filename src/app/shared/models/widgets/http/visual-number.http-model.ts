import { WidgetHttpModel, BaseMetadataHttpModel } from './common';
import { VisualNumberDataHttpModel } from './visual-number-data.http-model';

export interface VisualNumberHttpModel
  extends WidgetHttpModel<BaseMetadataHttpModel, VisualNumberDataHttpModel> {}
