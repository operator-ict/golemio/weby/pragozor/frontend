import { MetadataType } from './http/common/metadata.type';
import { CmsWidgetSettingsModel } from '../cms/cms-widget-settings.model';

export interface MetadataModel {
  id: string;
  refreshInterval: number;
  type: MetadataType;
  source: string;
  sourceUrl: string;
  unit: string;
  unitEN: string;
  updatedAt: Date;
  widgetSettings?: CmsWidgetSettingsModel;
}
