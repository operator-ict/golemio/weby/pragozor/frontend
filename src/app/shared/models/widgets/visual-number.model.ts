import { WidgetModel } from './widget.model';
import { CmsLocalizedTextModel } from '../cms';

export interface VisualNumberModel
  extends WidgetModel<{
    value: string;
    unit: string;
    label: CmsLocalizedTextModel;
  }> {}
