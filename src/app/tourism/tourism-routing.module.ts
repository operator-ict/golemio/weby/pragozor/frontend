import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TourismComponent } from './components/tourism/tourism.component';

const routes: Routes = [{ path: '', component: TourismComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TourismRoutingModule {}
